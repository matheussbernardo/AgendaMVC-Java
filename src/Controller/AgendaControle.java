package controller;

import java.util.Scanner;
import java.util.ArrayList;
import model.Pessoa;

public class AgendaControle {
	private ArrayList<Pessoa> listaPessoas;
	
	public AgendaControle() {
		listaPessoas = new ArrayList<Pessoa>();
	}
	
	public String adicionar(Pessoa umaPessoa) {
		String mensagem = "Pessoa adicionado com sucesso";
		listaPessoas.add(umaPessoa);
		return mensagem;
	}
	public void remover(Pessoa umaPessoa) {
		listaPessoas.remove(umaPessoa);
	}
	public Pessoa pesquisar(String umNome) {
		
		for(Pessoa umaPessoa: listaPessoas) {
			if(umaPessoa.getNome().equalsIgnoreCase(umNome))
				return umaPessoa;
		}
		return null;
	}
	public static String leString(){
		return new Scanner(System.in).nextLine().trim();
	}
	
	public static int leInt(){
		return new Scanner(System.in).nextInt();
	}
	
}