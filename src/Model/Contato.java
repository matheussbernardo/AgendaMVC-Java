package model;

public class Contato extends Pessoa {
	private String telefoneDeTrabalho;
	private String linkedin;
	
	public Contato(String nome, String telefoneDeTrabalho) {
		super(nome);
		this.telefoneDeTrabalho = telefoneDeTrabalho;
	}
	
	public String getTelefoneDeTrabalho() {
		return telefoneDeTrabalho;
	}
	public void setTelefoneDeTrabalho(String telefoneDeTrabalho) {
		this.telefoneDeTrabalho = telefoneDeTrabalho;
	}
	public String getLinkedin() {
		return linkedin;
	}
	public void setLinkedin(String linkedin) {
		this.linkedin = linkedin;
	}
	
	
}
