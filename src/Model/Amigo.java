package model;

public class Amigo extends Pessoa {
	private String Aniversario;
	private String Facebook;
	
	public Amigo(String nome) {
		super(nome);
	}
	
	public Amigo (String nome, String Aniversario){
		super(nome);
		this.Aniversario = Aniversario;
	}

	public String getAniversario() {
		return Aniversario;
	}

	public void setAniversario(String aniversario) {
		Aniversario = aniversario;
	}

	public String getFacebook() {
		return Facebook;
	}

	public void setFacebook(String facebook) {
		Facebook = facebook;
	}
	
	
	
}
